# Lightbreeze #

## A lightweight infrastructure installation and testing environment ##

## Disclaimer

This git repository only contains personal content, views and opinions
and is not affiliated with Red Hat in any way. Furthermore this is
totally unsupported work. It is not meant to be working or solve anything
you are facing or think or might have as a problem in your life.

## ATTENTION ##

This README is outdated and has to be adapted to the current situation of
the playbooks.

## Abstract ##

This project was initiated by me (j.thadden@redhat.com) during projects I
conducted at my employer Red Hat. The purpose is to produce a testing,
poc and learning environment on local hardware via virtualization or
(full fledged) containerization.

A part of it is showing a practical way to use heavy software (like
OpenStack) or annoying software (like Cisco AnyConnect) via container
technologies on insufficient or precious resources - for testing and
preparing customer environments - or just because we can :-)

This part is about embedding software that's normally running in VMs on
your weak Linux Laptop for testing things out. As an example we use
OpenStack (until now packstack), because if you know how this one works,
everything else is a breeze!

**Attention!** This project is not for deploying production environments.

It was originally developed for a lab at the OpenStack Summit 2018 in
Vancouver (https://github.com/jthadden/OpenStack_Summit_2018_Vancouver).

## Mirror ##

If you want to be independent from internet sources and probably speed up
everything a little bit, then use the mirror. To do that, you have to
*prepend* the mirror creation step by issuing the following as the first
step:

```
ansible-playbook -v -i hosts-OSSummit-minimal -e @config/config_OSSummit_minimal.yml -e @config/config_infrastructure_jo-laptop.yml --limit layer1,lxc-OSSummit-mirror,OSSummit-mirror --tags layer0,layer1,bootstrap,prep_hosts,mirror create.yml
```

Important variables:
mirror.maps			allows to map local directories into the container to survive the mirrored data over a destroy
repo_{files,packages},repo	defines what to mirror

Files:
/root/{{ inventory_hostname }}_mirror_prepared
				if this file is there the mirror will only resync, but not add new repos, so remove if yiou changed the config


## All ##

To make a minimal installation of all products after you deployed the mirror use:

```
ansible-playbook -v -i hosts-OSSummit-minimal -e @config/config_OSSummit_minimal.yml -e @config/config_infrastructure_jo-laptop.yml --skip-tags sync_mirror create.yml
```


## Stop ##

```
ansible-playbook -v -i hosts-OSSummit-minimal -e @config/config-OSSummit-minimal.yml -e @config/config_infrastructure_jo-laptop.yml stop.yml
```



## Start ##

```
ansible-playbook -v -i hosts-OSSummit-minimal -e @config/config-OSSummit-minimal.yml -e @config/config_infrastructure_jo-laptop.yml start.yml
```



## Destroy ##

The complete removal of the infrastructure can be done via:

```
ansible-playbook -v -i hosts-OSSummit-minimal -e @config/config_OSSummit_minimal.yml -e @config/config_infrastructure_jo-laptop.yml --tags layer0,layer1  destroy.yml
```
