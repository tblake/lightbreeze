#!/bin/bash
# this comes from: https://gist.github.com/carbonin/a25b84efca2e6b3c3f91b673821a22c8
# and was extended to remove the VG as well

# Stop and disable services
systemctl stop evmserverd
systemctl disable evmserverd

systemctl stop $APPLIANCE_PG_SERVICE
systemctl disable $APPLIANCE_PG_SERVICE

umount /dev/mapper/vg_pg-lv_pg
vgremove -y vg_pg
umount /dev/mapper/vg_miq_logs-lv_miq_logs
vgremove -y vg_miq_logs

# Remove auto-generated files
pushd /var/www/miq/vmdb
  rm -f REGION GUID certs/* config/database.yml
popd

# Remove database
pushd $APPLIANCE_PG_DATA
  rm -rf ./*
popd